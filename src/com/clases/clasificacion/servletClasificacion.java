package com.clases.clasificacion;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.clases.clasificacion.ClasificacionHandler;

@WebServlet("/servletClasificacion")
public class servletClasificacion extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (request.getParameter("idClasificacion") != null){
			
			Integer idCla = Integer.parseInt(request.getParameter("idClasificacion"));
			
			request.setAttribute("clasificacion", ClasificacionHandler.getInstance().getClasificacionById(idCla));
			request.setAttribute("idClasificacion", idCla);
			
			request.getRequestDispatcher("index.jsp").forward(request, response);
		}
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}

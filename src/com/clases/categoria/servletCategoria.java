package com.clases.categoria;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.clases.categoria.CategoriaHandler;


@WebServlet("/servletCategoria")
public class servletCategoria extends HttpServlet {
	private static final long serialVersionUID = 1L;

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (request.getParameter("idCategoria") != null){
			
			Integer idcat = Integer.parseInt(request.getParameter("idCategoria"));
			
			request.setAttribute("categoria", CategoriaHandler.getInstance().getCategoriaById(idcat));
			request.setAttribute("idCategoria", idcat);
			
			request.getRequestDispatcher("index.jsp").forward(request, response);
		}
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}

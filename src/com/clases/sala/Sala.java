package com.clases.sala;

public class Sala {
	private Integer idSala;
	private String nombre;
	private String tipo;
	private Integer idComplejo;
	public Integer getIdSala() {
		return idSala;
	}
	public void setIdSala(Integer idSala) {
		this.idSala = idSala;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public Integer getIdComplejo() {
		return idComplejo;
	}
	public void setIdComplejo(Integer idComplejo) {
		this.idComplejo = idComplejo;
	}
	public Sala(Integer idSala, String nombre, String tipo, Integer idComplejo) {
		super();
		this.idSala = idSala;
		this.nombre = nombre;
		this.tipo = tipo;
		this.idComplejo = idComplejo;
	}
	public Sala() {
		super();
	}
}

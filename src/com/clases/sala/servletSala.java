package com.clases.sala;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.clases.sala.SalaHandler;


@WebServlet("/servletSala")
public class servletSala extends HttpServlet {
	private static final long serialVersionUID = 1L;

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			if (request.getParameter("idSala") != null){
				
				Integer idSala = Integer.parseInt(request.getParameter("idSala"));
			
				request.setAttribute("sala", SalaHandler.getInstance().getSalaById(idSala));
				request.setAttribute("idSala", idSala);
			
				request.getRequestDispatcher("index.jsp").forward(request, response);
			}
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
	}

}

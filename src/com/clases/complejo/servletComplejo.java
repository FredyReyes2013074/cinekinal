package com.clases.complejo;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.clases.pelicula.PeliculaHandler;


@WebServlet("/servletComplejo")
public class servletComplejo extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("pelicula", PeliculaHandler.getInstance().getPeliculaList());
		request.setAttribute("pelicula1", PeliculaHandler.getInstance().getPeliculaById(1));
		request.setAttribute("pelicula2", PeliculaHandler.getInstance().getPeliculaById(2));
		request.setAttribute("pelicula3", PeliculaHandler.getInstance().getPeliculaById(3));
		request.setAttribute("pelicula4", PeliculaHandler.getInstance().getPeliculaById(4));
		request.setAttribute("pelicula5", PeliculaHandler.getInstance().getPeliculaById(5));
		request.setAttribute("pelicula6", PeliculaHandler.getInstance().getPeliculaById(6));
		request.setAttribute("pelicula7", PeliculaHandler.getInstance().getPeliculaById(7));
		request.setAttribute("pelicula8", PeliculaHandler.getInstance().getPeliculaById(8));
		request.setAttribute("pelicula9", PeliculaHandler.getInstance().getPeliculaById(9));
		request.setAttribute("pelicula10", PeliculaHandler.getInstance().getPeliculaById(10));
		request.setAttribute("pelicula11", PeliculaHandler.getInstance().getPeliculaById(11));
		request.setAttribute("pelicula12", PeliculaHandler.getInstance().getPeliculaById(12));
		request.setAttribute("pelicula13", PeliculaHandler.getInstance().getPeliculaById(13));
		request.setAttribute("pelicula14", PeliculaHandler.getInstance().getPeliculaById(14));
		request.setAttribute("pelicula15", PeliculaHandler.getInstance().getPeliculaById(15));
		
		
		
		
		request.getRequestDispatcher("carteleras.jsp").forward(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}

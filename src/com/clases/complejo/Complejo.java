package com.clases.complejo;

public class Complejo {
	private Integer idComplejo;
	private String nombre;
	private String direccion;
	private String cuidad;
	private String callCenter;
	private String latitud;
	private String longitud;
	private String mapa;
	public Integer getIdComplejo() {
		return idComplejo;
	}
	public void setIdComplejo(Integer idComplejo) {
		this.idComplejo = idComplejo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getCuidad() {
		return cuidad;
	}
	public void setCuidad(String cuidad) {
		this.cuidad = cuidad;
	}
	public String getCallCenter() {
		return callCenter;
	}
	public void setCallCenter(String callCenter) {
		this.callCenter = callCenter;
	}
	public String getLatitud() {
		return latitud;
	}
	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}
	public String getLongitud() {
		return longitud;
	}
	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}
	public String getMapa() {
		return mapa;
	}
	public void setMapa(String mapa) {
		this.mapa = mapa;
	}
	public Complejo(Integer idComplejo, String nombre, String direccion,
			String cuidad, String callCenter, String latitud, String longitud,
			String mapa) {
		super();
		this.idComplejo = idComplejo;
		this.nombre = nombre;
		this.direccion = direccion;
		this.cuidad = cuidad;
		this.callCenter = callCenter;
		this.latitud = latitud;
		this.longitud = longitud;
		this.mapa = mapa;
	}
	public Complejo() {
		super();
	}
	
}

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Cartelera</title>
		<link rel="stylesheet" type="text/css" href="style2.css">
		<link rel="stylesheet" type="text/css" href="estilos/style22.css">
		<link rel="stylesheet" type="text/css" href="estilos/style3.css">
		<link rel="stylesheet" type="text/css" href="estilos/style4.css">
		<script src="jquery-1.11.2.min.js" type="text/javascript"></script>
		<script src="script/cartelera.js" type="text/javascript"></script> 
		<script src="script/cartelera2.js" type="text/javascript"></script> 
		<script src="script/cartelera3.js" type="text/javascript"></script> 
		<script src="script/cartelera4.js" type="text/javascript"></script>
		<script src="script/cartelera5.js" type="text/javascript"></script> 
	</head>
	<body>
		
			<div id="filtros">
				<p id="p_filtro">Filtros</p>
			</div>
			
			<div id="tipoFiltros">
				<ul>
					<li id="li_uno">Fecha</li>
					<li><p id="li_dos">Complejo</p></li>
				</ul>
			</div>
			<div id="complejosfiltro"> 
				<p id="normalf" class="todos">Normal</p>
				<p id="plazaf"  class="todos">plaza</p>
				<p id="miraf"   class="todos">Mira</p>
				<p id="carreteraf" class="todos">Carretera</p>
				<p id="kinalf"  class="todos">Kinal</p>
			</div>
		<script >
			var mes=new Array ("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
			var dias= new Array ("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sabado");
			var f= new Date();
			
			document.write('<p id="fehcaA">'+dias[f.getDay()]+ ", "+f.getDate() + "de" + mes[f.getMonth()]+"de" + f.getFullYear()+'</p>');
		</script>
		
		
		<p id="complejo" class="plazatitulof"><a href="/MyWebsite/servletComplejo2?idComplejo=1">Plaza Atanasio</a></p>
		<div id="div_plaza">
			 <p id="tituloPeliculas" >${pelicula1.getNombre()}</p>
			 <p id="sala">Sala1</p>	
			 <div id="contenedorImagen">
			 	<a href="servletPelicula?idPelicula=1">
			 		<img id="img_carte"  src="${pelicula1.getImagen()}">
			 	</a>
			 </div>	 
			 <div id="contenedorHorari">
			 	<div id="div_horarioL">
			 		<p id="p_tituloshorario1">Lunes</p>
			 	</div>
			 	<div id="div_horarioM">
			 		<p id="p_tituloshorario2">Martes</p>
			 	</div>
			 	<div id="div_horarioMM">
			 		<p id="p_tituloshorario3">Miercoles</p>
			 	</div>
			 	<div id="div_horarioJ">
			 		<p id="p_tituloshorario4">Jueves</p>
			 	</div>
			 	<div id="div_horarioV">
			 		<p id="p_tituloshorario5">Viernes</p>
			 	</div>
			 	<p id="hora">10:00</p>
			 	<p id="hora1">13:00</p>
			 </div>
			 <p id="tituloPeliculas" >${pelicula2.getNombre()}</p>
			 <p id="sala">Sala2</p>	
			 <div id="contenedorImagen">
			 <a href="servletPelicula?idPelicula=2">
			 	<img id="img_carte"  src="${pelicula2.getImagen()}">
			 </a>
			 </div>	 
			 <div id="contenedorHorari">
			 	<div id="div_horarioL2">
			 		<p id="p_tituloshorario1">Lunes</p>
			 	</div>
			 	<div id="div_horarioM2">
			 		<p id="p_tituloshorario2">Martes</p>
			 	</div>
			 	<div id="div_horarioMM2">
			 		<p id="p_tituloshorario3">Miercoles</p>
			 	</div>
			 	<div id="div_horarioJ2">
			 		<p id="p_tituloshorario4">Jueves</p>
			 	</div>
			 	<div id="div_horarioV2">
			 		<p id="p_tituloshorario5">Viernes</p>
			 	</div>
			 	<p id="hora22">10:00</p>
			 	<p id="hora222">13:00</p>
			 </div>
			 <br />
			 <br />
			 <p id="tituloPeliculas" >${pelicula3.getNombre()}</p>
			 <p id="sala">Sala3</p>	
			 <div id="contenedorImagen">
			 <a href="servletPelicula?idPelicula=3">
			 	<img id="img_carte"  src="${pelicula3.getImagen()}">
			 </a>
			 </div>	 
			 <div id="contenedorHorari">
			 	<div id="div_horarioL3">
			 		<p id="p_tituloshorario1">Lunes</p>
			 	</div>
			 	<div id="div_horarioM3">
			 		<p id="p_tituloshorario2">Martes</p>
			 	</div>
			 	<div id="div_horarioMM3">
			 		<p id="p_tituloshorario3">Miercoles</p>
			 	</div>
			 	<div id="div_horarioJ3">
			 		<p id="p_tituloshorario4">Jueves</p>
			 	</div>
			 	<div id="div_horarioV3">
			 		<p id="p_tituloshorario5">Viernes</p>
			 	</div>
			 	<p id="hora3">10:00</p>
			 	<p id="hora33">13:00</p>
			 </div>
			 <br />
			 <br />
			 <p id="tituloPeliculas" >${pelicula4.getNombre()}</p>
			 <p id="sala">Sala5</p>	
			 <div id="contenedorImagen">
			 <a href="servletPelicula?idPelicula=4">
			 	<img id="img_carte"  src="${pelicula4.getImagen()}">
			 </a>
			 </div>	 
			 <div id="contenedorHorari">
			 	<div id="div_horarioL4">
			 		<p id="p_tituloshorario1">Lunes</p>
			 	</div>
			 	<div id="div_horarioM4">
			 		<p id="p_tituloshorario2">Martes</p>
			 	</div>
			 	<div id="div_horarioMM4">
			 		<p id="p_tituloshorario3">Miercoles</p>
			 	</div>
			 	<div id="div_horarioJ4">
			 		<p id="p_tituloshorario4">Jueves</p>
			 	</div>
			 	<div id="div_horarioV4">
			 		<p id="p_tituloshorario5">Viernes</p>
			 	</div>
			 	<p id="hora4">10:00</p>
			 	<p id="hora44">13:00</p>
			 </div>
			 <br />
			 <br />
		</div>
		<p id="complejo" class="kinaltitulof"><a href="/MyWebsite/servletComplejo2?idComplejo=2">Kinal</a></p>
		<div id="div_kinal">
		 <p id="tituloPeliculas" >${pelicula5.getNombre()}</p>
			 <p id="sala">Sala</p>	
			 <div id="contenedorImagen">
			 <a href="servletPelicula?idPelicula=5">
			 	<img id="img_carte"  src="${pelicula5.getImagen()}">
			 </a>
			 </div>	 
			 <div id="contenedorHorari">
			 	<div id="div_horarioL5">
			 		<p id="p_tituloshorario1">Lunes</p>
			 	</div>
			 	<div id="div_horarioM5">
			 		<p id="p_tituloshorario2">Martes</p>
			 	</div>
			 	<div id="div_horarioMM5">
			 		<p id="p_tituloshorario3">Miercoles</p>
			 	</div>
			 	<div id="div_horarioJ5">
			 		<p id="p_tituloshorario4">Jueves</p>
			 	</div>
			 	<div id="div_horarioV5">
			 		<p id="p_tituloshorario5">Viernes</p>
			 	</div>
			 	<p id="hora5">10:00</p>
			 	<p id="hora55">13:00</p>
			 </div>
			 <p id="tituloPeliculas" >${pelicula6.getNombre()}</p>
			 <p id="sala">Sala</p>	
			 <div id="contenedorImagen">
			 <a href="servletPelicula?idPelicula=6">
			 	<img id="img_carte"  src="${pelicula6.getImagen()}">
			 </a>
			 </div>	 
			 <div id="contenedorHorari">
			 	<div id="div_horarioL6">
			 		<p id="p_tituloshorario1">Lunes</p>
			 	</div>
			 	<div id="div_horarioM6">
			 		<p id="p_tituloshorario2">Martes</p>
			 	</div>
			 	<div id="div_horarioMM6">
			 		<p id="p_tituloshorario3">Miercoles</p>
			 	</div>
			 	<div id="div_horarioJ6">
			 		<p id="p_tituloshorario4">Jueves</p>
			 	</div>
			 	<div id="div_horarioV6">
			 		<p id="p_tituloshorario5">Viernes</p>
			 	</div>
			 	<p id="hora6">10:00</p>
			 	<p id="hora66">13:00</p>
			 </div>
			 <br />
			 <br />
			 <p id="tituloPeliculas" >${pelicula7.getNombre()}</p>
			 <p id="sala">Sala</p>	
			 <div id="contenedorImagen">
			 <a href="servletPelicula?idPelicula=7">
			 	<img id="img_carte"  src="${pelicula7.getImagen()}">
			 </a>
			 </div>	 
			 <div id="contenedorHorari">
			 	<div id="div_horarioL7">
			 		<p id="p_tituloshorario1">Lunes</p>
			 	</div>
			 	<div id="div_horarioM7">
			 		<p id="p_tituloshorario2">Martes</p>
			 	</div>
			 	<div id="div_horarioMM7">
			 		<p id="p_tituloshorario3">Miercoles</p>
			 	</div>
			 	<div id="div_horarioJ7">
			 		<p id="p_tituloshorario4">Jueves</p>
			 	</div>
			 	<div id="div_horarioV7">
			 		<p id="p_tituloshorario5">Viernes</p>
			 	</div>
			 	<p id="hora7">10:00</p>
			 	<p id="hora77">13:00</p>
			 </div>
			 <br />
			 <br />
			 <p id="tituloPeliculas" >${pelicula8.getNombre()}</p>
			 <p id="sala">Sala</p>	
			 <div id="contenedorImagen">
			 <a href="servletPelicula?idPelicula=8">
			 	<img id="img_carte"  src="${pelicula8.getImagen()}">
			 </a>
			 </div>	 
			 <div id="contenedorHorari">
			 	<div id="div_horarioL8">
			 		<p id="p_tituloshorario1">Lunes</p>
			 	</div>
			 	<div id="div_horarioM8">
			 		<p id="p_tituloshorario2">Martes</p>
			 	</div>
			 	<div id="div_horarioMM8">
			 		<p id="p_tituloshorario3">Miercoles</p>
			 	</div>
			 	<div id="div_horarioJ8">
			 		<p id="p_tituloshorario4">Jueves</p>
			 	</div>
			 	<div id="div_horarioV8">
			 		<p id="p_tituloshorario5">Viernes</p>
			 	</div>
			 	<p id="hora8">10:00</p>
			 	<p id="hora88">13:00</p>
			 </div>
			 <br />
			 <br />
		</div>
		<p id="complejo" class="miratitulof"><a href="/MyWebsite/servletComplejo2?idComplejo=3">Mira Flores</a></p>
		<div id="div_mira">
		<p id="tituloPeliculas" >${pelicula9.getNombre()}</p>
			 <p id="sala">Sala</p>	
			 <div id="contenedorImagen">
			 <a href="servletPelicula?idPelicula=9">
			 	<img id="img_carte"  src="${pelicula9.getImagen()}">
			 	</a>
			 </div>	 
			 <div id="contenedorHorari">
			 	<div id="div_horarioL9">
			 		<p id="p_tituloshorario1">Lunes</p>
			 	</div>
			 	<div id="div_horarioM9">
			 		<p id="p_tituloshorario2">Martes</p>
			 	</div>
			 	<div id="div_horarioMM9">
			 		<p id="p_tituloshorario3">Miercoles</p>
			 	</div>
			 	<div id="div_horarioJ9">
			 		<p id="p_tituloshorario4">Jueves</p>
			 	</div>
			 	<div id="div_horarioV9">
			 		<p id="p_tituloshorario5">Viernes</p>
			 	</div>
			 	<p id="hora9">10:00</p>
			 	<p id="hora99">13:00</p>
			 </div>
			 <p id="tituloPeliculas" >${pelicula10.getNombre()}</p>
			 <p id="sala">Sala</p>	
			 <div id="contenedorImagen">
			 <a href="servletPelicula?idPelicula=10">
			 	<img id="img_carte"  src="${pelicula10.getImagen()}">
			 </a>
			 </div>	 
			 <div id="contenedorHorari">
			 	<div id="div_horarioL10">
			 		<p id="p_tituloshorario1">Lunes</p>
			 	</div>
			 	<div id="div_horarioM10">
			 		<p id="p_tituloshorario2">Martes</p>
			 	</div>
			 	<div id="div_horarioMM10">
			 		<p id="p_tituloshorario3">Miercoles</p>
			 	</div>
			 	<div id="div_horarioJ10">
			 		<p id="p_tituloshorario4">Jueves</p>
			 	</div>
			 	<div id="div_horarioV10">
			 		<p id="p_tituloshorario5">Viernes</p>
			 	</div>
			 	<p id="hora10">10:00</p>
			 	<p id="hora101">13:00</p>
			 </div>
			 <br />
			 <br />
			 <p id="tituloPeliculas" >${pelicula11.getNombre()}</p>
			 <p id="sala">Sala</p>	
			 <div id="contenedorImagen">
			 <a href="servletPelicula?idPelicula=11">
			 	<img id="img_carte"  src="${pelicula11.getImagen()}">
			 </a>
			 </div>	 
			 <div id="contenedorHorari">
			 	<div id="div_horarioL11">
			 		<p id="p_tituloshorario1">Lunes</p>
			 	</div>
			 	<div id="div_horarioM11">
			 		<p id="p_tituloshorario2">Martes</p>
			 	</div>
			 	<div id="div_horarioMM11">
			 		<p id="p_tituloshorario3">Miercoles</p>
			 	</div>
			 	<div id="div_horarioJ11">
			 		<p id="p_tituloshorario4">Jueves</p>
			 	</div>
			 	<div id="div_horarioV11">
			 		<p id="p_tituloshorario5">Viernes</p>
			 	</div>
			 	<p id="hora11">10:00</p>
			 	<p id="hora111">13:00</p>
			 </div>
			 <br />
			 <br />
			 <p id="tituloPeliculas" >${pelicula12.getNombre()}</p>
			 <p id="sala">Sala</p>	
			 <div id="contenedorImagen">
			 <a href="servletPelicula?idPelicula=12">
			 	<img id="img_carte"  src="${pelicula12.getImagen()}">
			 </a>
			 </div>	 
			 <div id="contenedorHorari">
			 	<div id="div_horarioL12">
			 		<p id="p_tituloshorario1">Lunes</p>
			 	</div>
			 	<div id="div_horarioM12">
			 		<p id="p_tituloshorario2">Martes</p>
			 	</div>
			 	<div id="div_horarioMM12">
			 		<p id="p_tituloshorario3">Miercoles</p>
			 	</div>
			 	<div id="div_horarioJ12">
			 		<p id="p_tituloshorario4">Jueves</p>
			 	</div>
			 	<div id="div_horarioV12">
			 		<p id="p_tituloshorario5">Viernes</p>
			 	</div>
			 	<p id="hora12">10:00</p>
			 	<p id="hora122">13:00</p>
			 </div>
			 <br />
			 <br />
		</div>
		<p id="complejo" class="carretitulof"><a href="/MyWebsite/servletComplejo2?idComplejo=4">Carretera</a></p>
		<div id="div_carre">
		 <p id="tituloPeliculas" >${pelicula13.getNombre()}</p>
			 <p id="sala">Sala</p>	
			 <div id="contenedorImagen">
			 <a href="servletPelicula?idPelicula=13">
			 	<img id="img_carte"  src="${pelicula13.getImagen()}">
			 </a>
			 	
			 </div>	 
			 <div id="contenedorHorari">
			 	<div id="div_horarioL13">
			 		<p id="p_tituloshorario1">Lunes</p>
			 	</div>
			 	<div id="div_horarioM13">
			 		<p id="p_tituloshorario2">Martes</p>
			 	</div>
			 	<div id="div_horarioMM13">
			 		<p id="p_tituloshorario3">Miercoles</p>
			 	</div>
			 	<div id="div_horarioJ13">
			 		<p id="p_tituloshorario4">Jueves</p>
			 	</div>
			 	<div id="div_horarioV13">
			 		<p id="p_tituloshorario5">Viernes</p>
			 	</div>
			 	<p id="hora13">10:00</p>
			 	<p id="hora133">13:00</p>
			 </div>
			 <p id="tituloPeliculas" >${pelicula14.getNombre()}</p>
			 <p id="sala">Sala</p>	
			 <div id="contenedorImagen">
			 <a href="servletPelicula?idPelicula=14">
			 	<img id="img_carte"  src="${pelicula14.getImagen()}">
			 </a>
			 </div>	 
			 <div id="contenedorHorari">
			 	<div id="div_horarioL14">
			 		<p id="p_tituloshorario1">Lunes</p>
			 	</div>
			 	<div id="div_horarioM14">
			 		<p id="p_tituloshorario2">Martes</p>
			 	</div>
			 	<div id="div_horarioMM14">
			 		<p id="p_tituloshorario3">Miercoles</p>
			 	</div>
			 	<div id="div_horarioJ14">
			 		<p id="p_tituloshorario4">Jueves</p>
			 	</div>
			 	<div id="div_horarioV14">
			 		<p id="p_tituloshorario5">Viernes</p>
			 	</div>
			 	<p id="hora14">10:00</p>
			 	<p id="hora144">13:00</p>
			 </div>
			 <br />
			 <br />
			 <p id="tituloPeliculas" >${pelicula15.getNombre()}</p>
			 <p id="sala">Sala</p>	
			 <div id="contenedorImagen">
			 <a href="servletPelicula?idPelicula=15">
			 	<img id="img_carte"  src="${pelicula15.getImagen()}">
			 </a>
			 </div>	 
			 <div id="contenedorHorari">
			 	<div id="div_horarioL15">
			 		<p id="p_tituloshorario1">Lunes</p>
			 	</div>
			 	<div id="div_horarioM15">
			 		<p id="p_tituloshorario2">Martes</p>
			 	</div>
			 	<div id="div_horarioMM15">
			 		<p id="p_tituloshorario3">Miercoles</p>
			 	</div>
			 	<div id="div_horarioJ15">
			 		<p id="p_tituloshorario4">Jueves</p>
			 	</div>
			 	<div id="div_horarioV15">
			 		<p id="p_tituloshorario5">Viernes</p>
			 	</div>
			 	<p id="hora15">10:00</p>
			 	<p id="hora155">13:00</p>
			 </div>
			 <br />
			 <br />
			 
		</div>
	</body>
</html>
<!DOCTYPE HTML>
<html>
<head>
	<title>Pelicula</title>
	<script src="jquery-1.11.2.min.js" type="text/javascript"></script>   
	<script src="scripts/peli1.js" type="text/javascript"></script>
	<link rel="stylesheet" type="text/css" href="peliculas/imagenes/fondos/stylePropio1.css">
	<link rel="stylesheet" type="text/css" href="peliculas/stylePelis.css">
	<link rel="stylesheet" type="text/css" href="peliculas/stylePelis2.css">   
</head>
<body BACKGROUND="${pelicula.getImagen2()}">
	<embed src="audios/exodus.mp3" id="stop" >
	<div id="div_uno0">
		<div class="peli1" style="background-image:url(${pelicula.getImagen()})" >
			<div class="infoPeli1" >
			<p class="titulosDentropeli1">${pelicula.getNombre()}</p>
			<p class="fechaDentropeli1">Fecha</p>
			<p class="fechaDentropeli1">Inicio:1/1/2015</p>
			<p class="fechaDentropeli1">Fin:23/11/2015</p>
			</div>
		</div>
		<p class="Sinopsis" id="Descripcion0">Sinopsis</p>
		<p class="Descripcion" id="Descripcion0">${pelicula.getDescripcion()}</p>
		<div>
		<iframe class="youtube" type="text/html" style="margin:20px 300px 20px 20px" width="640" height="385" src="${pelicula.getTrailer()}" frameborder="0"></iframe>
		</div>

		<div id="lunes">
			<p id="pLunes">Horarios</p>
			<div class="horario1">
				<p class="p_Titulos1">Lunes</p>
				<p class="p_Titulos2">Martes</p>
				<p class="p_Titulos3">Miercoles</p>
				<p class="horarios" ><a href="/MyWebsite/horario?titulo=${pelicula.getNombre()} &horario=10:30"  class="links">10:30</a></p>
				<p class="horarios" ><a href="/MyWebsite/horario?titulo=${pelicula.getNombre()}&horario=19:10" class="links">19:10</a></p>
				<p class="horariosM1" ><a href="/MyWebsite/horario?titulo=${pelicula.getNombre()}&horario=11:15"  class="links">11:15</a></p>
				<p class="horariosM2" ><a href="/MyWebsite/horario?titulo=${pelicula.getNombre()}orario=14:20" class="links">14:20</a></p>
				<p class="horariosMi1" ><a href="/MyWebsite/horario?titulo=${pelicula.getNombre()}&horario=11:45"  class="links">11:45</a></p>
				<p class="horariosMi2" ><a href="/MyWebsite/horario?titulo=${pelicula.getNombre()}&horario=17:00"  class="links">17:00</a></p>
				<hr/>
			</div>
		</div>
		
	</div>
	
</body>
</html>
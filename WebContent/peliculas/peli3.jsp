<!DOCTYPE HTML>
<html>
<head>
	<title>Cantinflas</title>
	<link rel="stylesheet" type="text/css" href="style.css">
	<link rel="stylesheet" type="text/css" href="imagenes/fondos/stylePropio3.css">
	<link rel="stylesheet" type="text/css" href="stylePelis.css">

</head>
<body>
	<embed src="audios/cantinflas.mp3" id="stop" >
	</div>
	<div id="div_uno3">
		<div class="peli">
			<div class="infoPeli">
			<p class="titulosDentropeli">Cantinflas</p>
			<p class="fechaDentropeli">Fecha</p>
			<p class="fechaDentropeli">Inicio:10/12/2014</p>
			<p class="fechaDentropeli">Fin:23/12/2014</p>
			</div>
		</div>
		<p class="Sinopsis">Sinopsis</p>
		<p class="Descripcion">Mike Todd, un excéntrico productor de Broadway, llega a Los Ángeles con un proyecto de película bastante descabellado, La vuelta al mundo en 80 días, con el que quiere sacudir el star-system de Hollywood. Mario Moreno es un cómico que se gana la vida en las carpas de la Ciudad de México. Su personaje Cantinflas lo lleva a volverse un ícono del cine mexicano, y uno de los personajes más importantes de la industria fílmica.</p>
		<iframe class="youtube" type="text/html" style="margin:20px 300px 20px 20px" width="640" height="385" src="http://www.youtube.com/embed/F-7Zg0l6NKk" frameborder="0"></iframe>

		<div id="lunes">
			<p id="pLunes">Horarios</p>
			<div class="horario1">
				<p class="p_Titulos1">Lunes</p>
				<p class="p_Titulos2">Martes</p>
				<p class="p_Titulos3">Miercoles</p>
				<p class="horarios"><a href="/MyWebsite/horario?titulo=Cantinflas&horario=10:30" target="_blank" class="links">10:30</a></p>
				<p class="horarios"><a href="/MyWebsite/horario?titulo=Cantinflas&horario=19:10" target="_blank" class="links">19:10</a></p>
				<p class="horariosM1"><a href="/MyWebsite/horario?titulo=Cantinflas&horario=11:15" target="_blank" class="links">11:15</a></p>
				<p class="horariosM2"><a href="/MyWebsite/horario?titulo=Cantinflas&horario=14:20" target="_blank" class="links">14:20</a></p>
				<p class="horariosMi1"><a href="/MyWebsite/horario?titulo=Cantinflas&horario=11:45" target="_blank" class="links">11:45</a></p>
				<p class="horariosMi2"><a href="/MyWebsite/horario?titulo=Cantinflas&horario=17:00" target="_blank" class="links">17:00</a></p>
				<hr/>
			</div>
		</div>
	</div>
</body>
</html>
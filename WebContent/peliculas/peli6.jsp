<!DOCTYPE HTML>
<html>
<head>
	<title>Titanes del pacifico</title>
	<link rel="stylesheet" type="text/css" href="imagenes/fondos/stylePropio.css">
	<link rel="stylesheet" type="text/css" href="stylePelis.css">
	<link rel="stylesheet" type="text/css" href="stylePelis3.css">
</head>
<body>
	<embed src="audios/titanes.mp3" id="stop" >
	
	<div id="div_uno1">
		
		<div class="peli6">
			<div class="infoPeli6">
			<p class="titulosDentropeli6">Titanes del pacifico</p>
			<p class="fechaDentropeli6">Fecha</p>
			<p class="fechaDentropeli6">Inicio:1/1/2013</p>
			<p class="fechaDentropeli6">Fin:23/11/2013</p>
			</div>
		</div>
		<p class="Sinopsis">Sinopsis</p>
		<p class="Descripcion">El aclamado cineasta Guillermo del Toro llega, de Warner Bros. Pictures y Legendary Pictures, llega “Titanes Del Pacífico”.
			Cuando las legiones de criaturas monstruosas, conocidas como Kaiju, comienzan a subir desde el mar, empieza una guerra que acabará con millones de vidas, consumiendo todos recursos de la humanidad durante años y años.</p>
		<iframe class="youtube" type="text/html" style="margin:20px 300px 20px 20px" width="640" height="385" src="http://www.youtube.com/embed/4ekm20rSGs8" frameborder="0"></iframe>

		<div id="lunes">
			<p id="pLunes">Horarios</p>
			<div class="horario1">
				<p class="p_Titulos1">Lunes</p>
				<p class="p_Titulos2">Martes</p>
				<p class="p_Titulos3">Miercoles</p>
				<p class="horarios"><a href="/MyWebsite/horario?titulo=Pacific Rim&horario=10:30" target="_blank" class="links">10:30</a></p>
				<p class="horarios"><a href="/MyWebsite/horario?titulo=Pacific Rim&horario=19:10" target="_blank" class="links">19:10</a></p>
				<p class="horariosM1"><a href="/MyWebsite/horario?titulo=Pacific Rim&horario=11:15" target="_blank" class="links">11:15</a></p>
				<p class="horariosM2"><a href="/MyWebsite/horario?titulo=Pacific Rim&horario=14:20" target="_blank" class="links">14:20</a></p>
				<p class="horariosMi1"><a href="/MyWebsite/horario?titulo=Pacific Rim&horario=11:45" target="_blank" class="links">11:45</a></p>
				<p class="horariosMi2"><a href="/MyWebsite/horario?titulo=Pacific Rim&horario=17:00" target="_blank" class="links">17:00</a></p>
				<hr/>
			</div>
		</div>
		</div>
</body>
</html>
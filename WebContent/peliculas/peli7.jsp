<!DOCTYPE HTML>
<html>
<head>
	<title>Los ilusionistas</title>
	<link rel="stylesheet" type="text/css" href="imagenes/fondos/stylePropio7.css">
	<link rel="stylesheet" type="text/css" href="stylePelis.css">
	<link rel="stylesheet" type="text/css" href="stylePelis3.css">
</head>
<body>
	<embed src="audios/ilusionistas.mp3" id="stop" >
	<div id="div_uno7">

		<div class="peli7">
			<div class="infoPeli7">
			<p class="titulosDentropeli7">Los ilusionistas</p>
			<p class="fechaDentropeli7">Fecha</p>
			<p class="fechaDentropeli7">Inicio:1/2/2015</p>
			<p class="fechaDentropeli7">Fin:23/3/2015</p>
			</div>
		</div>
		<p class="Sinopsis">Sinopsis</p>
		<p class="Descripcion">Un equipo del FBI intenta capturar a "Los Cuatro Jinetes", un grupo formado por los mejores magos del mundo, quienes se dedican a robar bancos mientras realizan sus habituales espectáculos, repartiendo luego el dinero que obtienen entre el público. Dylan, un agente especial, está empeñado en capturarlos antes de que lleven a cabo el que parece será su mayor golpe.</p>
		<iframe class="youtube" type="text/html" style="margin:20px 300px 20px 20px" width="640" height="385" src="http://www.youtube.com/embed/EUWAvl9lUjQ" frameborder="0"></iframe>

		<div id="lunes">
			<p id="pLunes">Horarios</p>
			<div class="horario1">
				<p class="p_Titulos1">Lunes</p>
				<p class="p_Titulos2">Martes</p>
				<p class="p_Titulos3">Miercoles</p>
				<p class="horarios"><a href="/MyWebsite/horario?titulo=Ilusionistas&horario=10:30" target="_blank" class="links">10:30</a></p>
				<p class="horarios"><a href="/MyWebsite/horario?titulo=Ilusionistas&horario=19:10" target="_blank" class="links">19:10</a></p>
				<p class="horariosM1"><a href="/MyWebsite/horario?titulo=Ilusionistas&horario=11:15" target="_blank" class="links">11:15</a></p>
				<p class="horariosM2"><a href="/MyWebsite/horario?titulo=Ilusionistas&horario=14:20" target="_blank" class="links">14:20</a></p>
				<p class="horariosMi1"><a href="/MyWebsite/horario?titulo=Ilusionistas&horario=11:45" target="_blank" class="links">11:45</a></p>
				<p class="horariosMi2"><a href="/MyWebsite/horario?titulo=Ilusionistas&horario=17:00" target="_blank" class="links">17:00</a></p>
				<hr/>
			</div>
		</div>
	</div>
</body>
</html>
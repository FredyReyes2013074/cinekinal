<!DOCTYPE HTML>
<html>
<head>
	<title>Buscando a nemo</title>
	<link rel="stylesheet" type="text/css" href="imagenes/fondos/stylePropio5.css">
	<link rel="stylesheet" type="text/css" href="stylePelis.css">
	<link rel="stylesheet" type="text/css" href="stylePelis2.css">
</head>
<body>
	<embed src="audios/nemo.mp3" id="stop" >
	
	<div id="div_uno5">
		<div class="peli5">
			<div class="infoPeli5">
			<p class="titulosDentropeli5">Buscando a nemo</p>
			<p class="fechaDentropeli5">Fecha</p>
			<p class="fechaDentropeli5">Inicio:1/3/2015</p>
			<p class="fechaDentropeli5">Fin:23/3/2015</p>
			</div>
		</div>
		<p class="Sinopsis">Sinopsis</p>
		<p class="Descripcion">La película cuenta el increíble periplo de dos peces - Marlin y su hijo Nemo - que se ven obligados a separarse en la Gran Barrera de Coral, ya que Nemo es capturado por un buceador. El pobre termina en la pecera de la consulta de un dentista desde la que se divisa el puerto de Sydney.</p>
		<iframe class="youtube" type="text/html" style="margin:20px 300px 20px 20px" width="640" height="385" src="http://www.youtube.com/embed/X-Z-gQ8mKs0" frameborder="0"></iframe>

		<div id="lunes">
			<p id="pLunes">Horarios</p>
			<div class="horario1">
				<p class="p_Titulos1">Lunes</p>
				<p class="p_Titulos2">Martes</p>
				<p class="p_Titulos3">Miercoles</p>
				<p class="horarios"><a href="/MyWebsite/horario?titulo=Nemo&horario=10:30" target="_blank" class="links">10:30</a></p>
				<p class="horarios"><a href="/MyWebsite/horario?titulo=Nemo&horario=19:10" target="_blank" class="links">19:10</a></p>
				<p class="horariosM1"><a href="/MyWebsite/horario?titulo=Nemo&horario=11:15" target="_blank" class="links">11:15</a></p>
				<p class="horariosM2"><a href="/MyWebsite/horario?titulo=Nemo&horario=14:20" target="_blank" class="links">14:20</a></p>
				<p class="horariosMi1"><a href="/MyWebsite/horario?titulo=Nemo&horario=11:45" target="_blank" class="links">11:45</a></p>
				<p class="horariosMi2"><a href="/MyWebsite/horario?titulo=Nemo&horario=17:00" target="_blank" class="links">17:00</a></p>
				<hr/>
			</div>
		</div>
	</div>
</body>
</html>
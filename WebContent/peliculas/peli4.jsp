<!DOCTYPE HTML>
<html>
<head>
	<title>Titanic</title>
	<link rel="stylesheet" type="text/css" href="style.css">
	<link rel="stylesheet" type="text/css" href="imagenes/fondos/stylePropio4.css">
	<link rel="stylesheet" type="text/css" href="stylePelis.css">
	<link rel="stylesheet" type="text/css" href="stylePelis2.css">
</head>
<body>
	<embed src="audios/titanic.mp3" id="stop" >
	<div id="div_uno4">
		<div class="peli4">
			<div class="infoPeli4">
			<p class="titulosDentropeli4">Titanic</p>
			<p class="fechaDentropeli4">Fecha</p>
			<p class="fechaDentropeli4">Inicio:1/1/2000</p>
			<p class="fechaDentropeli4">Fin:23/11/2000</p>
			</div>
		</div>
		<p class="Sinopsis" id="sinopsis">Sinopsis</p>
		<p class="Descripcion" id="Descripcion4">Jack (DiCaprio), un joven artista, en una partida de cartas gana un pasaje para América, en el Titanic, el trasatlántico más grande y seguro jamás construido. A bordo, conoce a Rose (Kate Winslet), una joven de una buena familia venida a menos que va a contraer un matrimonio de conveniencia con Cal (Billy Zane), un millonario engreído a quien sólo interesa el prestigioso apellido de su prometida. Jack y Rose se enamoran, pero Cal y la madre de Rose ponen todo tipo de trabas a su relación. Inesperadamente, un inmenso iceberg pone en peligro la vida de los pasajeros..</p>
		<iframe class="youtube" type="text/html" style="margin:20px 300px 20px 20px" width="640" height="385" src="http://www.youtube.com/embed/zCy5WQ9S4c0" frameborder="0"></iframe>

		<div id="lunes">
			<p id="pLunes">Horarios</p>
			<div class="horario1">
				<p class="p_Titulos1">Lunes</p>
				<p class="p_Titulos2">Martes</p>
				<p class="p_Titulos3">Miercoles</p>
				<p class="horarios"><a href="/MyWebsite/horario?titulo=Titanic&horario=10:30" target="_blank" class="links">10:30</a></p>
				<p class="horarios"><a href="/MyWebsite/horario?titulo=Titanic&horario=19:10" target="_blank" class="links">19:10</a></p>
				<p class="horariosM1"><a href="/MyWebsite/horario?titulo=Titanic&horario=11:15" target="_blank" class="links">11:15</a></p>
				<p class="horariosM2"><a href="/MyWebsite/horario?titulo=Titanic&horario=14:20" target="_blank" class="links">14:20</a></p>
				<p class="horariosMi1"><a href="/MyWebsite/horario?titulo=Titanic&horario=11:45" target="_blank" class="links">11:45</a></p>
				<p class="horariosMi2"><a href="/MyWebsite/horario?titulo=Titanic&horario=17:00" target="_blank" class="links">17:00</a></p>
				<hr/>
			</div>
		</div>
	</div>
</body>
</html>
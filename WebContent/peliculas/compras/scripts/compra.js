$(document).ready(function(){
   		var contador=0;
   		var cont=0;
      var cont1=0;
      var cont2=0;
      var contadorAsientos=0;
      var contadorSeleccion=0;
      var mayores=0;
      var adultos=0;
      var chicos=0;
   		var naranja={ backgroundColor: 'orange' };
   		var blanco={backgroundColor: 'white' };
   		$(".botones").attr("disabled", true);

   		$("#reiniciar").click(function(argument) {
        $(".botones").attr("disabled", true);
        $("#mas").attr("disabled", false);
        $("#mas1").attr("disabled", false);
        $("#mas2").attr("disabled", false);
        $("#menos").attr("disabled", false);
        $("#menos1").attr("disabled", false);
        $("#menos2").attr("disabled", false);
        $("#total").text("0");
        $(".botones").css(blanco);
        $("#parrafoConteo").text("0");
        $("#parrafoConteo1").text("0");
        $("#parrafoConteo2").text("0");
        contadorSeleccion=0;
        contadorAsientos=0;
        cont=0;
        cont1=0;
        cont2=0;
        mayores=0;
        adultos=0;
        chicos=0;
      });

   		$(".botones").click(function(eventoss) {
        var back=$(this).css('background-color');
        if ( back== 'rgb(255, 255, 255)'){
          $(this).css(naranja);
          contadorSeleccion=contadorSeleccion+1;
          if(contadorSeleccion===contadorAsientos){
            $(".botones").attr("disabled", true);
            $("#mas").attr("disabled", true);
            $("#mas1").attr("disabled", true);
            $("#mas2").attr("disabled", true);
            $("#menos").attr("disabled", true);
            $("#menos1").attr("disabled", true);
            $("#menos2").attr("disabled", true);
          }
        }else{
          $(this).css(blanco);
          contadorSeleccion=contadorSeleccion-1;
        }
   		});

  		$("#mas").click(function(eventoC) {
  			cont=cont+1;
        contadorAsientos=contadorAsientos+1;
        mayores=mayores+30;
        $("#total").text(mayores+adultos+chicos);
  			$("#parrafoConteo").text(cont);
        $(".botones").attr("disabled", true);
        $(".botones").attr("disabled", false); 
  		});
  		$("#menos").click(function(eventoC) {
  			if(mayores>0){
          mayores=mayores-30;
          $("#total").text(mayores+adultos+chicos);
        };
  			if (cont>0) {
  			  cont=cont-1;	
          contadorAsientos=contadorAsientos-1;
          if (contadorAsientos===0) {
            $(".botones").attr("disabled", true);
          };
  			};
  			$("#parrafoConteo").text(cont);
        if(contadorSeleccion===contadorAsientos){
          $(".botones").attr("disabled", true);
        }
  		});

      $("#mas1").click(function(eventoCs) {
        cont1=cont1+1
        contadorAsientos=contadorAsientos+1;
        adultos=adultos+50;
        $("#total").text(mayores+adultos+chicos);
        $("#parrafoConteo1").text(cont1);
        $(".botones").attr("disabled", true);
        $(".botones").attr("disabled", false); 
      });
      $("#menos1").click(function(eventoCss) {
        if(adultos>0){
          adultos=adultos-50;
          $("#total").text(mayores+adultos+chicos);
        }
        if (cont1>0) {
          cont1=cont1-1; 
          contadorAsientos=contadorAsientos-1; 
          if (contadorAsientos===0) {
            $(".botones").attr("disabled", true);
          };
        };
        $("#parrafoConteo1").text(cont1);
        if(contadorSeleccion===contadorAsientos){
          $(".botones").attr("disabled", true);
        }
      });

      $("#mas2").click(function(eventoCsss) {
        cont2=cont2+1
        contadorAsientos=contadorAsientos+1;
        chicos=chicos+20;
        $("#total").text(mayores+adultos+chicos);
        $("#parrafoConteo2").text(cont2);
        $(".botones").attr("disabled", true);
        $(".botones").attr("disabled", false); 
      });
      $("#menos2").click(function(eventoCssss) {
        if(chicos>0){
          chicos=chicos-20;
          $("#total").text(mayores+adultos+chicos);
        };
        if (cont2>0) {
          cont2=cont2-1;  
          contadorAsientos=contadorAsientos-1;
          if (contadorAsientos===0) {
            $(".botones").attr("disabled", true);
          };
        };
        $("#parrafoConteo2").text(cont2);
        if(contadorSeleccion===contadorAsientos){
          $(".botones").attr("disabled", true);
        }
      });

	});
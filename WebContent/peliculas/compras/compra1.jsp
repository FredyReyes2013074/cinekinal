<!DOCTYPE HTML>
<html>
	<head>
		<title>Comprar</title>
		<link rel="stylesheet" type="text/css" href="peliculas/compras/estilos/compra1.css">
		<script src="peliculas/compras/jquery-1.11.2.min.js" type="text/javascript"></script>   
		<script src="peliculas/compras/scripts/compra.js" type="text/javascript"></script>   
		
	</head>
	<body>
		<script language="JavaScript" type="text/javascript">

		</script>
		<div id="div_titulohorario">
		<p id="tituloPeli">${titulo}</p>
		<p id="horarioPeli">${horario}</p>
		</div>
		<div id="asientos">
		<img id="pantalla" src="peliculas/compras/estilos/pantalla.png"><br />
		<% 
  	
  			 int fila = 0;
  	 		 int columna = 0;
  			 for(int y=1; y<=5; y++){
  				for(int x=1; x<=16; x++){
  	
  		  %>
  	
  			<button class="botones" fila="<%=y%>" columna="<%=x%>"></button>
  
  			<%
  				}
  					}	
  			%>
		
		</div> 	
		<div id="seleccionAsientos">
		<p id="Asientos">Asientos</p>
		<p id="mayores">Adulstos mayores</p>
		<button id="menos"><p id="parrafoBoton1">-</p></button>
		<button id="conteo"><p id="parrafoConteo">0</p></button>
		<button id="mas"><p id="parrafoBoton2">+</p></button>
		<p class="precio">Q 30.00</p>
		<br/>
		<p id="adultos">Adultos</p>
		<button id="menos1"><p id="parrafoBoton3">-</p></button>
		<button id="conteo1"><p id="parrafoConteo1">0</p></button>
		<button id="mas1"><p id="parrafoBoton4">+</p></button>
		<p class="precio">Q 50.00</p>
		<br/>
		<p id="chicos">Chicos</p>
		<button id="menos2"><p id="parrafoBoton5">-</p></button>
		<button id="conteo2"><p id="parrafoConteo2">0</p></button>
		<button id="mas2"><p id="parrafoBoton6">+</p></button>
		<p class="precio">Q 20.00</p>
		</div>
		<p id="precio">Precio:</p>
		<p id="total">0</p>
		<button id="reiniciar"><p id="p_reiniciar">Reiniciar</p></button>
		<div class="siguiente" >
			<a  href="peliculas/compras/pago.jsp?titulo=${titulo}">
			<img id="img_Pago" src="peliculas/compras/estilos/pago.gif">
			</a>
		</div>
	</body>
</html>
<!DOCTYPE HTML>
<html>
<head>
	<title>Voces Inocentes</title>
	<link rel="stylesheet" type="text/css" href="imagenes/fondos/stylePropio2.css">
	<link rel="stylesheet" type="text/css" href="style.css">
	<link rel="stylesheet" type="text/css" href="stylePelis.css">
	<link rel="stylesheet" type="text/css" href="stylePelis2.css">
</head>
<body>
	<embed src="audios/voces.mp3" id="stop" >
	<div id="div_uno2">
		<div class="peli2">
			<div class="infoPeli2">
			<p class="titulosDentropeli2">Voces Inocentes</p>
			<p class="fechaDentropeli2">Fecha</p>
			<p class="fechaDentropeli2">Inicio:1/1/2010</p>
			<p class="fechaDentropeli2">Fin:23/11/2010</p>
			</div>
		</div>
		<p class="Sinopsis">Sinopsis</p>
		<p class="Descripcion">Cuenta la historia de Chava (Carlos Padilla), un niño de once años que atrapado por las circunstancias tiene que convertirse en «el hombre de la casa», después de que su padre los abandonara en plena Guerra Civil. Durante la década de los años ochenta en El Salvador, las fuerzas armadas del gobierno reclutaban niños de doce años sacándolos de sus escuelas. Si Chava tiene suerte, aún le queda un año de inocencia, un año antes de que él también sea enrolado y luche la batalla del gobierno contra los rebeldes del ejército FMLN (Frente Farabundo Martí para la Liberación Nacional).</p>
		<iframe class="youtube" type="text/html" style="margin:20px 300px 20px 20px" width="640" height="385" src="http://www.youtube.com/embed/hRaMgKbb3Z4" frameborder="0"></iframe>

		<div id="lunes">
			<p id="pLunes">Horarios</p>
			<div class="horario1">
				<p class="p_Titulos1">Lunes</p>
				<p class="p_Titulos2">Martes</p>
				<p class="p_Titulos3">Miercoles</p>
				<p class="horarios"><a href="/MyWebsite/horario?titulo=Voces Inocentes&horario=10:30" target="_blank" class="links">10:30</a></p>
				<p class="horarios"><a href="/MyWebsite/horario?titulo=Voces Inocentes&horario=19:10" target="_blank" class="links">19:10</a></p>
				<p class="horariosM1"><a href="/MyWebsite/horario?titulo=Voces Inocentes&horario=11:15" target="_blank" class="links">11:15</a></p>
				<p class="horariosM2"><a href="/MyWebsite/horario?titulo=Voces Inocentes&horario=14:20" target="_blank" class="links">14:20</a></p>
				<p class="horariosMi1"><a href="/MyWebsite/horario?titulo=Voces Inocentes&horario=11:45" target="_blank" class="links">11:45</a></p>
				<p class="horariosMi2"><a href="/MyWebsite/horario?titulo=Voces Inocentes&horario=17:00" target="_blank" class="links">17:00</a></p>
				<hr/>
			</div>
		</div>
	</div>
</body>
</html>
$(document).ready(function(){
	
		$("#div_horarioL13").click(function(argument) {
			$("#hora13").text("10:00");
			$("#hora133").text("13:00");
			$("#hora13").css("margin-left",0);
			$("#hora133").css("margin-left",0);
		});
		$("#div_horarioM13").click(function(argument) {
			$("#hora13").text("12:45");
			$("#hora133").text("18:55");
			$("#hora13").css("margin-left",140);
			$("#hora133").css("margin-left",140);
        });
		$("#div_horarioMM13").click(function(argument) {
			$("#hora13").text("17:25");
			$("#hora133").text("21:25");
			$("#hora13").css("margin-left",280);
			$("#hora133").css("margin-left",280);
        });
		$("#div_horarioJ13").click(function(argument) {
			$("#hora13").text("14:35");
			$("#hora133").text("19:45");
			$("#hora13").css("margin-left",420);
			$("#hora133").css("margin-left",420);
        });
		$("#div_horarioV13").click(function(argument) {
			$("#hora13").text("11:55");
			$("#hora133").text("17:35");
			$("#hora13").css("margin-left",560);
			$("#hora133").css("margin-left",560);
        });

		$("#div_horarioL14").click(function(argument) {
			$("#hora14").text("10:00");
			$("#hora144").text("13:00");
			$("#hora14").css("margin-left",0);
			$("#hora144").css("margin-left",0);
		});
		$("#div_horarioM14").click(function(argument) {
			$("#hora14").text("14:15");
			$("#hora144").text("17:05");
			$("#hora14").css("margin-left",140);
			$("#hora144").css("margin-left",140);
        });
		$("#div_horarioMM14").click(function(argument) {
			$("#hora14").text("20:15");
			$("#hora144").text("23:45");
			$("#hora14").css("margin-left",280);
			$("#hora144").css("margin-left",280);
        });
		$("#div_horarioJ14").click(function(argument) {
			$("#hora14").text("14:35");
			$("#hora144").text("15:55");
			$("#hora14").css("margin-left",420);
			$("#hora144").css("margin-left",420);
        });
		$("#div_horarioV14").click(function(argument) {
			$("#hora14").text("11:35");
			$("#hora144").text("16:35");
			$("#hora14").css("margin-left",560);
			$("#hora144").css("margin-left",560);
        });
		
		$("#div_horarioL15").click(function(argument) {
			$("#hora15").text("10:00");
			$("#hora155").text("13:00");
			$("#hora15").css("margin-left",0);
			$("#hora155").css("margin-left",0);
		});
		$("#div_horarioM15").click(function(argument) {
			$("#hora15").text("12:15");
			$("#hora155").text("14:25");
			$("#hora15").css("margin-left",140);
			$("#hora155").css("margin-left",140);
        });
		$("#div_horarioMM15").click(function(argument) {
			$("#hora15").text("11:55");
			$("#hora155").text("15:15");
			$("#hora15").css("margin-left",280);
			$("#hora155").css("margin-left",280);
        });
		$("#div_horarioJ15").click(function(argument) {
			$("#hora15").text("11:25");
			$("#hora155").text("16:35");
			$("#hora15").css("margin-left",420);
			$("#hora155").css("margin-left",420);
        });
		$("#div_horarioV15").click(function(argument) {
			$("#hora15").text("10:05");
			$("#hora155").text("15:35");
			$("#hora15").css("margin-left",560);
			$("#hora155").css("margin-left",560);
        });
});